<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->string('exam_name');
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->bigInteger('grade_id')->index()->unsigned();
            $table->enum('exam_type',['online','offline']);
            $table->string('exam_start_date');
            $table->string('exam_start_time');
            $table->string('exam_end_time');
            $table->bigInteger('school_id')->index()->unsigned();
            $table->bigInteger('creater_id')->index()->unsigned();
            $table->unique(["subject_id", "grade_id","exam_name"], 'school_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
