<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('grade_id')->index()->unsigned();
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->string('group_name');
            $table->integer('weight');
            $table->bigInteger('school_id')->index()->unsigned();
            $table->bigInteger('creater_id')->index()->unsigned();
            $table->enum('status',['1','0']);
            $table->unique(["subject_id", "grade_id","school_id"], 'group_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
