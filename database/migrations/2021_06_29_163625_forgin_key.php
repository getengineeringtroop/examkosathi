<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForginKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Users Table
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');

        });
        //  Exam Table
         Schema::table('exams', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');

        });
        // Exam Table
        Schema::table('questions', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
        // Grade Table
        Schema::table('grades', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
        });
        // //Subject Table
        Schema::table('subjects', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
        });
        // Group Table
        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
        // Group Question Table
        Schema::table('group_questions', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
        // Group Question Table
        Schema::table('exam_groups', function (Blueprint $table) {
            $table->foreign('group_question_id')->references('id')->on('group_questions')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
        });
        // Student Exam Table
        Schema::table('student_exams', function (Blueprint $table) {
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
        });
        // Student Answer Table
        Schema::table('studen_answers', function (Blueprint $table) {
            $table->foreign('student_exam_id')->references('id')->on('student_exams')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
        // Report Table
        Schema::table('reports', function (Blueprint $table) {
            $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');

        });
        //Feedback Table
        Schema::table('feedback', function (Blueprint $table) {
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');

        });
        //Subscriptin Table
        Schema::table('user_subscriptions', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
        //Discussion Question Table
        Schema::table('discussion_questions', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->foreign('post_user_id')->references('id')->on('users')->onDelete('cascade');
        });
         //Discussion Table
         Schema::table('discussions', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('discussion_questions_id')->references('id')->on('discussion_questions')->onDelete('cascade');
            $table->foreign('replay_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
