<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussion_questions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->bigInteger('grade_id')->index()->unsigned();
            $table->bigInteger('question_id')->index()->unsigned();
            $table->bigInteger('exam_id')->index()->unsigned();
            $table->bigInteger('post_user_id')->index()->unsigned();
            $table->text('question');
            $table->bigInteger('school_id')->index()->unsigned();
            $table->enum('status',['1','0']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussion_questions');
    }
}
