<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->text('question');
            $table->bigInteger('grade_id')->index()->unsigned();
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->enum('question_type',['mcsa','mcma','tof','sa','mtf','so','fitb']);
            $table->text('answer1')->nullable();
            $table->text('answer2')->nullable();
            $table->text('answer3')->nullable();
            $table->text('answer4')->nullable();
            $table->text('answer5')->nullable();
            $table->text('answer6')->nullable();
            $table->text('right_answer_1')->nullable();
            $table->text('right_answer_2')->nullable();
            $table->text('right_answer_3')->nullable();
            $table->text('right_answer_4')->nullable();
            $table->text('right_answer_5')->nullable();
            $table->text('right_answer_6')->nullable();
            $table->enum('status',['1','0']);
            $table->bigInteger('school_id')->index()->unsigned();
            $table->unique(['grade_id','subject_id','school_id'],'question');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
