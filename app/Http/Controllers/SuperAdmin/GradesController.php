<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\GradesResource;
use App\Models\Grade;
use Illuminate\Http\Request;

class GradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::where('status', '1')->paginate(10);
        if ($grades==null) {
            abort(
                response()->json(['message' => 'Grade Not Found'], 404)
            );
        }
        return GradesResource::collection($grades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $grades = new Grade;
            $grades->grader_name = $request->grader_name;
            $grades->description = $request->description;
            $grades->school_id = $request->school_id;
            $grades->status = $request->status;
            $grades->save();
            return new GradesResource($grades);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grades = Grade::where('id', $id)->first();
            
       if($grades==null){
            abort(
                response()->json(['message' => 'Object Not Found'], 404)
            );
       }
       return new GradesResource($grades);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $grades = Grade::find($id);
            $grades->grader_name = $request->grader_name;
            $grades->description = $request->description;
            $grades->school_id = $request->school_id;
            $grades->status = $request->status;
            $grades->update();
            return new GradesResource($grades);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $grade = Grade::findOrFail($id);
            $grade->delete();
            return new GradesResource($grade);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
    
}
