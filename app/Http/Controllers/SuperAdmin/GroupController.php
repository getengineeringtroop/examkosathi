<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\GroupResource;
use App\Models\Grade;
use App\Models\Group;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::where('status', '1')->paginate(10);
        if (count($groups)==0) {
            abort(
                response()->json(['message' => 'Group Not Found'], 404)
            );
        }
        return SubjectResource::collection($groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $group = new Group;
            $group->grade_id = $request->grade_id;
            $group->subject_id = $request->subject_id;
            $group->group_name = $request->group_name;
            $group->weight = $request->weight;
            $group->school_id = $request->school_id;
            $group->creater_id = $request->creater_id;
            $group->status = $request->status;
            $group->create();
            return new GroupResource($group);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::where('id', $id)->first();
        $grade=Grade::all();
        $subject=Subject::all();
            
       if($group==null){
            abort(
                response()->json(['message' => 'Object Not Found'], 404)
            );
       }
       return new PermissionResource($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $group = Group::find($id);
            $group->grade_id = $request->grade_id;
            $group->subject_id = $request->subject_id;
            $group->group_name = $request->group_name;
            $group->weight = $request->weight;
            $group->school_id = $request->school_id;
            $group->creater_id = $request->creater_id;
            $group->status = $request->status;
            $group->update();
            return new GroupResource($group);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $group = Group::findOrFail($id);
            $group->delete();
            return new GroupResource($group);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
}
