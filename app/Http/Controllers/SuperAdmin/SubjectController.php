<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\SubjectResource;
use App\Models\Grade;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::where('status', '1')->paginate(10);
        if (count($subjects)==0) {
            abort(
                response()->json(['message' => 'Subject Not Found'], 404)
            );
        }
        return SubjectResource::collection($subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $subject = new Subject;
            $subject->subject_name = $request->subject_name;
            $subject->description = $request->description;
            $subject->school_id = $request->school_id;
            $subject->status = $request->status;
            $subject->save();
            return new SubjectResource($subject);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::where('id', $id)->first();
            
       if($subject==null){
            abort(
                response()->json(['message' => 'Object Not Found'], 404)
            );
       }
       return new SubjectResource($subject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $subject = Subject::find($id);
            $subject->subject_name = $request->subject_name;
            $subject->description = $request->description;
            $subject->school_id = $request->school_id;
            $subject->status = $request->status;
            $subject->update();
            return new SubjectResource($subject);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to update data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $subject = Subject::findOrFail($id);
            $subject->delete();
            return new SubjectResource($subject);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
}
