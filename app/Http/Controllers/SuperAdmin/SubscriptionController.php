<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\SubscriptionResource;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscription = Subscription::where('status', '1')->paginate(10);
        if (count($subscription)==0) {
            abort(
                response()->json(['message' => 'Subscription Not Found'], 404)
            );
        }
        return SubjectResource::collection($subscription);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $subscription = new Subscription;
            $subscription->subscription_name = $request->subscription_name;
            $subscription->number_of_exam = $request->number_of_exam;
            $subscription->price = $request->price;
            $subscription->status = $request->status;
            $subscription->update();
            return new SubscriptionResource($subscription);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to update data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = Subscription::where('id', $id)->first();
            
       if($subscription==null){
            abort(
                response()->json(['message' => 'Object Not Found'], 404)
            );
       }
       return new SubscriptionResource($subscription);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $subscription = Subscription::find($id);
            $subscription->subscription_name = $request->subscription_name;
            $subscription->number_of_exam = $request->number_of_exam;
            $subscription->price = $request->price;
            $subscription->status = $request->status;
            $subscription->update();
            return new SubscriptionResource($subscription);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to update data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $subscription = Subscription::findOrFail($id);
            $subscription->delete();
            return new SubscriptionResource($subscription);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
    
}
