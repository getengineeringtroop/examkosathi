<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\UserResource;
use App\Models\Grade;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Foundation\Auth\User as IlluminateUser;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles', 'grades')->where('status', '1')->paginate(10);
        if ($users==null) {
            abort(
                response()->json(['message' => 'user Not Found'], 404)
            );
        }
        return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $grade = Grade::all();
        $data = [
            'roles' => $roles,
            'grade' => $grade,
        ];
        if ($data==null) {
            abort(
                response()->json(['message' => 'user Not Found'], 404)
            );
        }
        return UserResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $users = new User;
            $users->name = $request->name;
            $users->email = $request->email;
            $users->password = Hash::make($request->password);
            $users->number = $request->number;
            $users->grader = $request->grader;
            $users->school_id = $request->school_id;
            $users->status = $request->status;
            $users->role_id = $request->role_id;
            if (isset($users->image)) {
                $file = $users->image;
                $file_name = microtime() . '.' . $file->getClientOriginalExtension();
                $users->move(public_path('images/user/'), $file_name);
                $users->image = $file_name;
            }
            $users->save();
            return new UserResource($users);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to create data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $users = User::with('roles', 'grades')->where('id', $id)->first();
            if($users==null){
                abort(
                    response()->json(['message' => 'user Not Found'], 404)
                );
            }
             return new UserResource($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $roles = Role::all();
            $grade = Grade::all();
            $users = User::with('roles', 'grades')->where('id', $id)->first();
            $data = [
                'roles' => $roles,
                'grade' => $grade,
                'user' => $users
            ];
            return UserResource::collection($data);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'Object Not Found'], 404)
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $users = User::find($id);
            $users->name = $request->name;
            $users->email = $request->email;
            $users->password = Hash::make($request->password);
            $users->number = $request->number;
            $users->grader = $request->grader;
            $users->school_id = $request->school_id;
            $users->status = $request->status;
            $users->role_id = $request->role_id;
            if (isset($users->image)) {
                $file = $users->image;
                $file_name = microtime() . '.' . $file->getClientOriginalExtension();
                $users->move(public_path('images/user/'), $file_name);
                $users->image = $file_name;
            }
            $users->update();
            return new UserResource($users);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to Update data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return new UserResource($user);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
}
