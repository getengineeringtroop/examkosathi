<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SuperAdmin\ExamResource;
use App\Models\Exam;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::where('status', '1')->paginate(10);
        if (count($exams)==0) {
            abort(
                response()->json(['message' => 'Exam Not Found'], 404)
            );
        }
        return ExamResource::collection($exams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subject=Subject::all();
        $grade=Grade::all();
        $school=User::whereNull('school_id', null);
        $create=User::all();
        $data=[
            'subject' => $subject,
            'grade' => $grade,
            'school' => $school,
            'create' => $create
        ];
        return ExamResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $exam = new Exam;
            $exam->exam_name = $request->exam_name;
            $exam->subject_id = $request->subject_id;
            $exam->grade_id = $request->grade_id;
            $exam->exam_type = $request->exam_type;
            $exam->exam_start_date = $request->exam_start_date;
            $exam->exam_end_time = $request->exam_end_time;
            $exam->school_id = $request->school_id;
            $exam->creater_id = $request->creater_id;
            $exam->status = $request->status;
            $exam->save();
            return new ExamResource($exam);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to update data'], 404)
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $exam = Exam::find($id);
            $exam->exam_name = $request->exam_name;
            $exam->subject_id = $request->subject_id;
            $exam->grade_id = $request->grade_id;
            $exam->exam_type = $request->exam_type;
            $exam->exam_start_date = $request->exam_start_date;
            $exam->exam_end_time = $request->exam_end_time;
            $exam->school_id = $request->school_id;
            $exam->creater_id = $request->creater_id;
            $exam->status = $request->status;
            $exam->update();
            return new ExamResource($exam);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to update data'], 404)
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try {
            $exam = Exam::findOrFail($id);
            $exam->delete();
            return new ExamResource($exam);
        } catch (\Exception $e) {
            abort(
                response()->json(['message' => 'error to delete data'], 404)
            );
        }
    }
    
}
