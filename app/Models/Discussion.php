<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    use HasFactory;
    
    public function discussionQuestionAnswer(){
        return $this->belongsTo(DiscussionQuestion::class,'discussion_questions_id');
    }
    public function discussionReplyUser(){
        return $this->belongsTo(User::class,'replay_user_id');
    }
    public function discussionReplySchool(){
        return $this->belongsTo(User::class,'school_id');
    }
    

}
