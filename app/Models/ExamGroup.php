<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamGroup extends Model
{
    use HasFactory;
    public function groupQuestionExam(){
        return $this->belongsTo(GroupQuestion::class, 'group_id');
    }
    public function examGroupExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
}
