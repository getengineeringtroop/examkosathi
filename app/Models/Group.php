<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    public function groupGrade(){
        return $this->belongsTo(Grade::class, 'grade_id');
    }
    public function groupSubject(){
        return $this->belongsTo(Subject::class,'subject_id');
    }
    public function groupSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
    public function group(){
        return $this->hasMany(GroupQuestion::class);
    }
}
