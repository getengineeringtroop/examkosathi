<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupQuestion extends Model
{
    use HasFactory;
    public function group(){
        return $this->belongsTo(Group::class, 'group_id');
    }
    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
    public function groupQuestionExam(){
        return $this->hasMany(ExamGroup::class);
    }
}
