<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class studenAnswer extends Model
{
    use HasFactory;
    public function studentAnswerStudent(){
        return $this->belongsTo(User::class,'student_id');
    }
    public function studentAnswerExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
    public function studentAnswewrQuestion(){
        return $this->belongsTo(Question::class,'question_id');
    }
    
}
