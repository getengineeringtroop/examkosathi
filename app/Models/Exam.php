<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;
    public function examGrade(){
        return $this->belongsTo(Grade::class, 'grade_id');
    }
    public function examSubject(){
        return $this->belongsTo(Subject::class, 'subject_id');
    }
    public function examSchool(){
        return $this->belongsTo(User::class, 'school_id');
    }
    public function examGroupExam(){
        return $this->hasMany(ExamGroup::class);
    }
    public function studentExam(){
        return $this->hasMany(studentExam::class);
    }
    public function studentAnswerExam(){
        return $this->hasMany(studenAnswer::class);
    }
    public function reportExam(){
        return $this->hasMany(Report::class);
    }
    public function feedbackExam(){
        return $this->hasMany(Feedback::class);
    }
    public function discussionExam(){
        return $this->hasMany(DiscussionQuestion::class);
    }
}
