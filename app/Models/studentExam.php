<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class studentExam extends Model
{
    use HasFactory;
    public function studentStudent(){
        return $this->belongsTo(User::class,'student_id');
    }
    public function studentExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
    public function studentSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
}
