<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;
    public function feedbackExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
    public function feedbackQuestion(){
        return $this->belongsTo(Question::class,'question_id');
    }
    public function feedbackSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
}
