<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscussionQuestion extends Model
{
    use HasFactory;
    public function discussinSubject(){
        return $this->belongsTo(Subject::class,'subject_id');
    }
    public function discussionGrade(){
        return $this->belongsTo(Subject::class,'subject_id');
    }
    public function discussionQuestion(){
        return $this->belongsTo(Question::class,'question_id');
    }
    public function createrUser(){
        return $this->belongsTo(User::class,'post_user_id');
    }
    public function discussionExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
    public function discussionSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
    public function discussionQuestionAnswer(){
        return $this->hasMany(Discussion::class);
    }
}
