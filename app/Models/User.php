<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function roles(){
        return $this->belongsTo(Role::class,'role_id');
    }
    public function grades(){
        return $this->belongsTo(Grade::class,'grade_id');
    }
    public function userPermission(){
        return $this->hasMany(UserPermission::class);
    }
    public function examSchool(){
        return $this->hasMany(Exam::class);
    }
    public function questionSchool(){
        return $this->hasMany(Question::class);
    }
    public function schoolGrade(){
        return $this->hasMany(Grade::class);
    }
    public function schoolSubject(){
        return $this->hasMany(Subject::class);
    }
    public function groupSchool(){
        return $this->hasMany(Group::class);
    }
    public function studentSchool(){
        return $this->hasMany(studentExam::class);
    }
    public function studentStudent(){
        return $this->hasMany(studentExam::class);
    }
    public function studentAnswerStudent(){
        return $this->hasMany(studenAnswer::class);
    }
    public function reportStudent(){
        return $this->hasMany(Report::class);
    }
    public function reportSchool(){
        return $this->hasMany(Report::class);
    }
    public function feedbackSchool(){
        return $this->hasMany(Feedback::class);
    }
    public function subscriptionUser(){
        return $this->hasMany(UserSubscription::class);
    }
    public function createrUser(){
        return $this->hasMany(DiscussionQuestion::class);
    }
    public function discussionSchool(){
        return $this->hasMany(DiscussionQuestion::class);
    }
    public function discussionReplyUser(){
        return $this->hasMany(Discussion::class);
    }
    public function discussionReplySchool(){
        return $this->hasMany(Discussion::class);
    }
}
