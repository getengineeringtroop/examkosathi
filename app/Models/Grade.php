<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use HasFactory;
    public function grades(){
        return $this->hasMany(User::class);
    } 
    public function examGrade(){
        return $this->hasMany(Exam::class);
    }
    public function questionGrade(){
        return $this->hasMany(Question::class);
    }
    public function schoolGrade(){
        return $this->belongsTo(User::class, 'school_id');
    }
    public function groupGrade(){
        return $this->hasMany(Group::class);
    }
    public function discussionGrade(){
        return $this->hasMany(DiscussionQuestion::class);
    }
}
