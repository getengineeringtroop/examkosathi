<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    public function questionGrade(){
        return $this->belongsTo(Grade::class, 'grader_id');
    }
    public function questionSubject(){
        return $this->belongsTo(Subject::class,'subject_id');
    }
    public function questionSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
    public function question(){
        return $this->hasMany(GroupQuestion::class);
    }
    public function studentAnswewrQuestion(){
        return $this->hasMany(studenAnswer::class);
    }
    public function feedbackQuestion(){
        return $this->hasMany(Feedback::class);
    }
    public function discussionQuestion(){
        return $this->hasMany(DiscussionQuestion::class);
    }
}
