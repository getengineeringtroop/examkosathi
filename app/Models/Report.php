<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    public function reportStudent(){
        return $this->belongsTo(User::class,'student_id');
    }
    public function reportSchool(){
        return $this->belongsTo(User::class,'school_id');
    }
    public function reportExam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }
}
