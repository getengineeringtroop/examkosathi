<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    public function examSubject(){
        return $this->hasMany(Exam::class);
    }
    public function questionSubject(){
        return $this->hasMany(Question::class);
    }
    public function schoolSubject(){
        return $this->belongsTo(User::class,'school_id');
    }
    public function groupSubject(){
        return $this->hasMany(Group::class);
    }
    public function discussinSubject(){
        return $this->hasMany(DiscussionQuestion::class);
    }
}
