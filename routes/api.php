<?php

use App\Http\Controllers\SuperAdmin\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('*', function(){
    abort(
        response()->json(['message' => 'Page Not Found'], 404)
    );
});
//User Route
Route::get('users', [\App\Http\Controllers\SuperAdmin\UserController::class ,'index'])->name('users.index');
Route::post('users/store', [\App\Http\Controllers\SuperAdmin\UserController::class ,'store'])->name('users.store');
Route::get('users/create', [\App\Http\Controllers\SuperAdmin\UserController::class ,'create'])->name('users.create');
Route::get('users/show/{id}', [\App\Http\Controllers\SuperAdmin\UserController::class ,'show'])->name('users.show');
Route::get('users/edit/{id}', [\App\Http\Controllers\SuperAdmin\UserController::class ,'edit'])->name('users.edit');
Route::get('users/delete/{id}', [\App\Http\Controllers\SuperAdmin\UserController::class ,'destroy'])->name('users.delete');
Route::post('users/update/{id}', [\App\Http\Controllers\SuperAdmin\UserController::class ,'update'])->name('users.update');

//Grade Route
Route::get('grades', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'index'])->name('grades.index');
Route::post('grades/store', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'store'])->name('grades.store');
Route::get('grades/create', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'create'])->name('grades.create');
Route::get('grades/show/{id}', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'show'])->name('grades.show');
Route::get('grades/edit/{id}', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'edit'])->name('grades.edit');
Route::get('grades/delete/{id}', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'destroy'])->name('grades.delete');
Route::post('grades/update/{id}', [\App\Http\Controllers\SuperAdmin\GradesController::class ,'update'])->name('grades.update');

//Subject Route
Route::get('subjects', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'index'])->name('subjects.index');
Route::post('subjects/store', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'store'])->name('subjects.store');
Route::get('subjects/create', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'create'])->name('subjects.create');
Route::get('subjects/show/{id}', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'show'])->name('subjects.show');
Route::get('subjects/edit/{id}', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'edit'])->name('subjects.edit');
Route::get('subjects/delete/{id}', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'destroy'])->name('subjects.delete');
Route::post('subjects/update/{id}', [\App\Http\Controllers\SuperAdmin\SubjectController::class ,'update'])->name('subjects.update');
//Roles Route
Route::get('roles', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'index'])->name('roles.index');
Route::post('roles/store', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'store'])->name('roles.store');
Route::get('roles/create', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'create'])->name('roles.create');
Route::get('roles/show/{id}', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'show'])->name('roles.show');
Route::get('roles/edit/{id}', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'edit'])->name('roles.edit');
Route::get('roles/delete/{id}', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'destroy'])->name('roles.delete');
Route::post('roles/update/{id}', [\App\Http\Controllers\SuperAdmin\RoleController::class ,'update'])->name('roles.update');
//Permission Route
Route::get('permissions', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'index'])->name('permissions.index');
Route::post('permissions/store', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'store'])->name('permissions.store');
Route::get('permissions/create', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'create'])->name('permissions.create');
Route::get('permissions/show/{id}', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'show'])->name('permissions.show');
Route::get('permissions/edit/{id}', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'edit'])->name('permissions.edit');
Route::get('permissions/delete/{id}', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'destroy'])->name('permissions.delete');
Route::post('permissions/update/{id}', [\App\Http\Controllers\SuperAdmin\PermissionController::class ,'update'])->name('permissions.update');
//Group Route
Route::get('groups', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'index'])->name('groups.index');
Route::post('groups/store', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'store'])->name('groups.store');
Route::get('groups/create', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'create'])->name('groups.create');
Route::get('groups/show/{id}', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'show'])->name('groups.show');
Route::get('groups/edit/{id}', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'edit'])->name('groups.edit');
Route::get('groups/delete/{id}', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'destroy'])->name('groups.delete');
Route::post('groups/update/{id}', [\App\Http\Controllers\SuperAdmin\GroupController::class ,'update'])->name('groups.update');
//Subscription Route
Route::get('subscriptions', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'index'])->name('subscriptions.index');
Route::post('subscriptions/store', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'store'])->name('subscriptions.store');
Route::get('subscriptions/create', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'create'])->name('subscriptions.create');
Route::get('subscriptions/show/{id}', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'show'])->name('subscriptions.show');
Route::get('subscriptions/edit/{id}', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'edit'])->name('subscriptions.edit');
Route::get('subscriptions/delete/{id}', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'destroy'])->name('subscriptions.delete');
Route::post('subscriptions/update/{id}', [\App\Http\Controllers\SuperAdmin\SubscriptionController::class ,'update'])->name('subscriptions.update');
//Exam Route
Route::get('exams', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'index'])->name('exams.index');
Route::post('exams/store', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'store'])->name('exams.store');
Route::get('exams/create', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'create'])->name('exams.create');
Route::get('exams/show/{id}', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'show'])->name('exams.show');
Route::get('exams/edit/{id}', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'edit'])->name('exams.edit');
Route::get('exams/delete/{id}', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'destroy'])->name('exams.delete');
Route::post('exams/update/{id}', [\App\Http\Controllers\SuperAdmin\ExamController::class ,'update'])->name('exams.update');




